'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  //BASE_API: '"http://47.96.100.107:55682"'
  BASE_API: '"http://localhost:8000"'
  // BASE_API: '"https://api.auauz.net"'
})
