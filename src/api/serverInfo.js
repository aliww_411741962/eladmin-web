import request from '@/utils/request'

export function add(data) {
  return request({
    url: 'api/serverInfo',
    method: 'post',
    data
  })
}

export function del(id) {
  return request({
    url: 'api/serverInfo/' + id,
    method: 'delete'
  })
}

export function edit(data) {
  return request({
    url: 'api/serverInfo',
    method: 'put',
    data
  })
}

export function downloadServerInfo(params) {
  return request({
    url: 'api/serverInfo/download',
    method: 'get',
    params,
    responseType: 'blob'
  })
}
